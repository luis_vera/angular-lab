import { Component, OnInit } from '@angular/core';
import { Product } from './../models/product.models';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  constructor() {
    this.products = [
      {name:'Samsumg A10',category:'Celular',description:'Entra Ahora Al Sitio Oficial Samsung ® México Y Descubre Más Del Galaxy S10. Sé Parte De La Experiencia Galaxy Con El Nuevo S10'}
      ,{name:'Portátil ASUS X509FJ-BR092T',category:'Laptop',description:'ASUS X509 es el computador portátil liviano que ofrece un rendimiento potente y efectos visuales envolventes.'}
    ];
  }

  ngOnInit(): void {
  }

  guardar(name:string, category:string, description:string):boolean{
    this.products.push( new Product(name, category, description) );
    console.log( this.products );
    return false;
  }

}
