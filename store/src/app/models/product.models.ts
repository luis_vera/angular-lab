export class Product {
  name: string;
  category: string;
  description: string;

  constructor(n:string, c:string, d:string) {
    this.name = n;
    this.category = c;
    this.description = d;
  }
}
